Simple asp core project as a web api working with vagrant file.

Basically install vagrant https://www.vagrantup.com/downloads.html, but what's vagrant?  
Vagrant allow you to create and configure lightweight, reproducible, and portable development environments.

To make vagrant works you need a virtual machine, I can recomend to install Virtual Box 
https://www.virtualbox.org/wiki/Downloads 

After install vagrant, and virtualbox, create an asp core project, inserting the vagrant file on project root, open the prompt, and run the commands:
- vagrant plugin install vagrant-hostmanager
- vagrant up

And wait install windowsserver 2012.